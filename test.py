import numpy as np
import pandas as pd
from squeezenet import SqueezeNet
from keras.applications.imagenet_utils import preprocess_input, decode_predictions
from keras.preprocessing import image

model = SqueezeNet()

TOP_ONE = 0
TOP_FIVE = 0
PREDICTION_SIZE = 30000

labels = pd.read_csv("LOC_val_solution.csv", sep=',')
IdToResult = {}
for i in range(len(labels['ImageId'])):
    IdToResult[labels['ImageId'][i]] = labels['PredictionString'][i].split(' ')[0]

print('Start predicting: ')
for i in range(PREDICTION_SIZE):
    if (i + 1) % 1000 == 0:
        print((i+1)*100/PREDICTION_SIZE, '%')
    if i+1 < 10:
        num = '0000' + str(i+1)
    elif i+1 < 100:
        num = '000' + str(i+1)
    elif i+1 < 1000:
        num = '00' + str(i+1)
    elif i+1 < 10000:
        num = '0' + str(i+1)
    else:
        num = str(i+1)
    filename = r"ILSVRC2012_val_000" + num
    filename_with_path = 'val\\' + filename + ".jpeg"
    img = image.load_img(filename_with_path, target_size=(227, 227))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    preds = decode_predictions(model.predict(x))
    pred_as_str = preds[0][0][0]
    other_four = [preds[0][1][0], preds[0][2][0], preds[0][3][0], preds[0][4][0]]

    if pred_as_str == IdToResult[filename]:
        TOP_ONE += 1
        TOP_FIVE += 1
    else:
        for j in range(len(other_four)):
            if other_four[j] == IdToResult[filename]:
                TOP_FIVE += 1
                break

print('Top1 accuracy: ', TOP_ONE/PREDICTION_SIZE)
print('Top5 accuracy: ', TOP_FIVE/PREDICTION_SIZE)